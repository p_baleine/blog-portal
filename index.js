/**
 * Module dependencies.
 */

var express = require('express')
  , Resource = require('express-resource')
  , mongoose = require('mongoose');

// setup middleware

var app = module.exports = express();
app.use(express.static(__dirname + '/public'));
app.use(app.router);

/**
 * config
 */

app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

app.configure('development', function() {
  app.set('mongo conn', 'mongodb://localhost:27017/blog-portal');
});

mongoose.connect(app.set('mongo conn'));

app.get('/', function(req, res) {
  res.render('index', { content: 'Hello world' });
});

/**
 * resources
 */

app.resource('recommends', require('./resources/recommends'));

if (!module.parent) {
  app.listen(3000);
  console.log('Express started on port 3000');
}