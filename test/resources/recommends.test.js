var express = require('express')
  , request = require('supertest')
  , sinon = require('sinon')
  , mockery = require('mockery')
  , Resource = require('express-resource');

var app = express();

/**
 * mocks
 */

var Recommend = {
  find: function(cb) {
    cb(null, [{ title: 'JavaScript' }]);
  },

  findOne: function() { return this; },
  populate: function() { return this; },
  exec: function(cb) {
    cb(null, { title: 'JS', posts: [{ title: 'Prototype' }, { title: 'Function' }] });
  }
};

describe('recommends', function() {
  before(function() {
    mockery.warnOnUnregistered(false);
    mockery.registerMock('../models/recommend', Recommend);
    mockery.enable();
    app.resource('recommends', require('../../resources/recommends'));
  });

  after(function() {
    mockery.deregisterMock('../models/recommend');
    mockery.disable();
  });

  it ('GET /recommends', function(done) {
    request(app)
        .get('/recommends')
        .expect('Content-Type', /json/)
        .expect([{ title: 'JavaScript' }])
        .expect(200, done);
  });

  it('GET /recommends/:recommend', function(done) {
    request(app)
        .get('/recommends/12345')
        .expect('Content-Type', /json/)
        .expect({ title: 'JS', posts: [{ title: 'Prototype' }, { title: 'Function' }] })
        .expect(200, done);
  });
});