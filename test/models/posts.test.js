var should = require('chai').should()
  , mongoose = require('mongoose')
  , ObjectId = mongoose.Types.ObjectId
  , Post = require('../../models/post');

describe('Post', function() {
  before(function(done) {
    mongoose.connect('mongodb://localhost/blog-portal-test', function() {
      Post.remove(done);
    });
  });

  describe('creation', function() {
    before(function(done) {
      var self = this;

      this.recommend = new ObjectId();
      this.post = new Post({
        recommend: this.recommend,
        title: 'Introduction to On-Page Guidance',
        summary: 'My name is Oded Ben-David',
        date: new Date(2012, 11, 2),
        link: 'http://feedproxy.google.com/~r/dailyjs/~3/3qCW-UHAcO8/on-screen-guidance-intro',
        guid: 'http://dailyjs.com/2012/11/02/on-screen-guidance-intro',
        image: [
          {
            url: 'hoge',
            title: 'piyo'
          }
        ]
      });

      this.post.save(function(err) {
        Post.findOne({ _id: self.post._id }, function(err, saved) {
          self.saved = saved;
          done(err);
        });
      });
    });

    it('should save title', function() {
      this.saved.title.should.have.string(this.post.title);
    });

    it('should have reference to its recommend', function() {
      this.saved.recommend.toString().should.equal(this.recommend.toString());
    });
  });
});
