var should = require('chai').should()
  , mongoose = require('mongoose')
  , ObjectId = mongoose.Types.ObjectId
  , Recommend = require('../../models/recommend');

describe('Recommend', function() {
  before(function(done) {
    mongoose.connect('mongodb://localhost/blog-portal-test', function() {
      Recommend.remove(done);
    });
  });

  describe('creation', function() {
    before(function(done) {
      var self = this;

      this.posts = [new ObjectId(), new ObjectId()];

      this.recommend = new Recommend({
        title: 'JavaScript',
        description: 'My Favorite JavaScript Feeds',
        posts: this.posts
      });

      this.recommend.save(function(err) {
        Recommend.findOne({ _id: self.recommend._id }, function(err, saved) {
          self.saved = saved;
          done();
        });
      });
    });

    it('should save title', function() {
      this.saved.title.should.have.string('JavaScript');
    });

    it('should save description', function() {
      this.saved.description.should.have.string('My Favorite JavaScript Feeds');
    });

    it('should save references to posts', function() {
      this.saved.posts[0].toString().should.equal(this.posts[0].toString());
      this.saved.posts[1].toString().should.equal(this.posts[1].toString());
    });

    it('should not create Recommend without title', function(done) {
      var recommend = new Recommend();

      recommend.save(function(err) {
        err.message.should.equal('Validation failed');
        done();
      });
    });
  });
});
