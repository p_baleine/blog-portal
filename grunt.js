module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: '<json:package.json>',
    lint: {
      files: [
        'grunt.js',
        'index.js',
        'model/**/*.js',
        'resources/**/*.js',
        'task/**/*.js',
        'test/**/*.js',
        'client/*.js',
        'client/local/*/*.js'
      ]
    },
    watch: {
      files: '<config:lint.files>',
      tasks: 'default'
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        boss: true,
        eqnull: true,
        node: true,
        laxcomma: true,
        es5: true,
        strict: false
      },
      globals: {
        exports: true,
        describe: false,
        it: false,
        before: false,
        after: false,
        chai: false,
        fixture: false,
        sinon: false
      }
    },
    simplemocha: {
      all: {
        src: ['test/**/*.js'],
        options: {
          globals: ['should'],
          timeout: 3000,
          ignoreLeaks: false,
          ui: 'bdd',
          reporter: 'spec'
        }
      }
    },
    component: {
      include: "client",
      componentPath: "client/local"
    },
    testacularServer: {
      unit: {
        configFile: 'client/testacular.conf.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-simple-mocha');
  grunt.loadNpmTasks('grunt-component');
  grunt.loadNpmTasks('grunt-testacular');

  // Default task.
  grunt.registerTask('default', 'lint simplemocha');

};
