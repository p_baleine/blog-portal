var mongoose = require('mongoose')
  , feedparser = require('feedparser')
  , S = require('string')
  , Recommend = require('../models/recommend')
  , Post = require('../models/post');

module.exports = function(grunt) {

  grunt.registerTask('crawl', 'crawl the registered feeds', function() {
    var done = this.async();

    mongoose.connect('mongodb://localhost/blog-portal', function() {
      Recommend.find(function(err, recommends) {
        var feeds = grunt.utils._.flatten(recommends.map(function(r) {
          return r.feeds.map(function(f) { return { recommend: r, url: f }; });
        }));

        feeds = feeds.map(function(feed) {
          return function(cb) {
            feedparser.parseUrl(feed.url, function(err, meta, articles) {
              function save(i) {
                var article = articles[i]
                  , post = new Post({
                    title: article.title,
                    image: article.image.url,
                    description: article.description,
                    summary: article.summary || S(article.description).stripTags().truncate(200).s,
                    link: article.link,
                    recommend: feed.recommend._id
                  });

                post.save(function(err, saved) {
                  feed.recommend.posts.push(saved);
                  feed.recommend.save(function(err, saved) {
                    i += 1;

                    if (i == articles.length) {
                      cb();
                    } else {
                      save(i);
                    }
                  });
                });
              }

              save(0);
            });
          };
        });

        grunt.utils.async.parallel(feeds, done);
      });
    });
  });
};