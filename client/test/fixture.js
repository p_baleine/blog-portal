;(function() {

  var root = this;

  root.fixture = {
    recommend : {
      title: 'DailyJS',
      posts: [
        {
          title: 'Connecting Fuel UX Datagrid to the Flickr API',
          summary: 'Connecting Fuel UX',
          link: 'http://google.com'
        },
        {
          title: 'Dynamic Invocation Part 1: Methods',
          summary: 'Dynamic Invocation',
          link: 'http://google.com'
        }
      ]
    }
  };

}.call(this));