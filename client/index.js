var $ = require('jquery')
  , Backbone = require('backbone.js')
  , recommend = require('recommend');

Backbone.$ = $;

module.exports = recommend;
