var _ = require('underscore')
  , Backbone = require('backbone.js');

var preview = module.exports = _.extend({}, Backbone.Events);

/**
 * Preview view who display summary of post
 */

preview.PreviewView = Backbone.View.extend({

  TIMEOUT: 1000,

  render: function(model) {
    this.$el.append(this.template(model.toJSON()));

    if (model.get('image')) {
      this.$el.css({
        'background-image': 'url(' + model.get('image') + ')'
      });
    }

    return this;
  },

  // update el if specified timeout is passed.
  update: function(model) {
    clearTimeout(this.timer);

    this.timer = setTimeout(_.bind(function() {
      this.$el.empty();
      this.render(model);
    }, this), this.TIMEOUT);
  },

  template: _.template(require('./template.js'))
});