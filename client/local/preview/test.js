var should = chai.should()
  , $ = require('component-jquery')
  , Backbone = require('solutionio-backbone/index.js')
  , preview = require('preview');

describe('recommend', function() {
  beforeEach(function() {
    Backbone.$ = $;
  });

  describe('PreviewView', function() {
    beforeEach(function() {
      this.post = new Backbone.Model({
        title: 'Connecting Fuel UX',
        summary: 'Fuel UX (GitHub: ExactTarget / fuelux, License: MIT) extends',
        image: 'http://dailyjs.com/images/posts/fuel-ux-flickrgrid.png',
        link: 'http://google.com'
      });
      this.view = new preview.PreviewView();
      this.view.render(this.post);
    });

    it('should render title', function() {
      this.view.$('.title').text().should.have.string('Connecting Fuel UX');
    });
  });
});
