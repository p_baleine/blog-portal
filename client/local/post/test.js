var should = chai.should()
  , Backbone = require('solutionio-backbone/index.js')
  , post = require('post');

describe('post', function() {
  it('should implement Backbone.Event interface', function() {
    post.on.should.be.a('function');
    post.trigger.should.be.a('function');
  });

  describe('PostView', function() {
    beforeEach(function() {
      this.model = new Backbone.Model(fixture.recommend.posts[0]);
      this.view = new post.PostView({ model: this.model });
    });

    it('should render post title', function() {
      this.view.render().$el.text().should.have.string(
        'Connecting Fuel UX Datagrid to the Flickr API'
      );
    });

    it('should have `post` class', function() {
      this.view.render().$el.should.have.class('post');
    });

    it('should trigger post:attention when mouseenter event is fired', function() {
      this.view.should.trigger('post:attention', { with: [this.model] }).when(function() {
        this.view.$el.trigger('mouseenter');
      }.bind(this));
    });
  });
});