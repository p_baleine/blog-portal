var _ = require('underscore')
  , Backbone = require('backbone.js');

var post = module.exports = _.extend({}, Backbone.Events);

post.PostSet = Backbone.Collection.extend();

post.PostView = Backbone.View.extend({
  tagName: 'li',

  className: 'post',

  events: {
    'mouseenter': 'attention'
  },

  render: function() {
    this.$el.append(this.template(this.model.toJSON()));

    return this;
  },

  attention: function() {
    this.trigger('post:attention', this.model);
  },

  template: _.template(require('./template.js'))
});