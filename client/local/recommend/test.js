var should = chai.should()
  , $ = require('component-jquery')
  , Backbone = require('solutionio-backbone/index.js')
  , recommend = require('recommend')
  , post = require('post')
  , preview = require('preview');

describe('recommend', function() {
  beforeEach(function() {
    Backbone.$ = $;
  });

  it('should implement Backbone.Event interface', function() {
    recommend.on.should.be.a('function');
    recommend.trigger.should.be.a('function');
  });

  describe('Recommend', function() {
    beforeEach(function() {
      this.fetchSpy = sinon.spy(recommend.Recommend.prototype, 'fetch');
      this.PostSetSpy = sinon.spy(post, 'PostSet');
      this.server = sinon.fakeServer.create();
      this.server.respondWith('GET', '/recommends', [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(fixture.recommend)
      ]);
      this.model = new recommend.Recommend();
      this.server.respond();
    });

    afterEach(function() {
      this.fetchSpy.restore();
      this.PostSetSpy.restore();
      this.server.restore();
    });

    it('should fetch data on initialization', function() {
      this.fetchSpy.should.have.been.calledOnce;
    });

    it('should store posts as post.PostSet when data is returned', function() {
      this.PostSetSpy.should.have.been.calledOnce;
      this.PostSetSpy.should.have.been.calledWith(fixture.recommend.posts);
    });
  });

  describe('RecommendView', function() {
    beforeEach(function() {
      this.RecommendStub = sinon.stub(recommend, 'Recommend');
      this.model = new Backbone.Model(fixture.recommend);
      this.model.posts = new Backbone.Collection(fixture.recommend.posts);
      this.RecommendStub.returns(this.model);
      this.PostViewSpy = sinon.spy(post, 'PostView');
      this.PreviewViewSpy = sinon.spy(preview, 'PreviewView');
      this.PreviewRenderSpy = sinon.spy(preview.PreviewView.prototype, 'render');
      this.args = { id: 111 };
      this.view = new recommend.RecommendView(this.args);
    });

    afterEach(function() {
      recommend.Recommend.restore();
      post.PostView.restore();
      preview.PreviewView.restore();
      this.PreviewRenderSpy.restore();
    });

    it('should pass id to its model', function() {
      this.RecommendStub.should.have.been.calledWith({ _id: 111 });
    });

    it('should be an instance of Backbone.View', function() {
      this.view.should.be.an.instanceof(Backbone.View);
    });

    it('should render recommend title', function() {
      this.view.render().$('h2').should.have.text('DailyJS');
    });

    it('should render recommends\' posts', function() {
      this.view.render();
      this.PostViewSpy.should.have.been.calledTwice;
    });

    it('should instantiate preview', function() {
      this.view.render();
      this.PreviewViewSpy.should.have.been.calledOnce;
    });

    it('should render preview with first post', function() {
      this.view.render();
      this.PreviewRenderSpy.should.have.been.calledWith(this.model.posts.first());
    });

    // TODO specs for preview update
  });
});