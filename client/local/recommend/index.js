var _ = require('underscore')
  , Backbone = require('backbone.js')
  , post = require('post')
  , preview = require('preview');

var recommend = module.exports = _.extend({}, Backbone.Events);

/**
 * Recommend view who contains Post views
 */

recommend.RecommendView = Backbone.View.extend({
  initialize: function() {
    this.recommend = new recommend.Recommend({ _id: this.id });
    this.recommend.on('change', this.render, this);
  },

  render: function() {
    var params = this.recommend.toJSON();

    // delegate rendering of posts to post.PostView
    delete params['posts'];
    this.$el.append(this.template(params));

    // preview
    this.preview = new preview.PreviewView({ el: this.$('.preview') });
    this.preview.render(this.recommend.posts.first());

    this.recommend.posts.each(function(post) {
      this.renderPost(post);
    }, this);

    return this;
  },

  renderPost: function(model) {
    var postView = new post.PostView({ model: model });

    postView.on('post:attention', this.updatePreview, this);
    this.$('.posts').append(postView.render().el);
  },

  updatePreview: function(model) {
    this.preview.update(model);
  },

  template: _.template(require('./template.js'))
});

/**
 * Recommend model which consits of Post model
 */

recommend.Recommend = Backbone.Model.extend({
  urlRoot: '/recommends',

  idAttribute: '_id',

  initialize: function() {
    this.on('change', this.populatePosts, this);
    this.fetch();
  },

  populatePosts: function() {
    this.posts = new post.PostSet(this.get('posts'), { recommend: this });
  }
});
