/**
 * module dependencies
 */

var Recommend = require('../models/recommend')
  , Post = require('../models/post');

exports.index = function(req, res) {
  Recommend.find(function(err, recommends) {
    res.send(recommends);
  });
};

exports.show = function(req, res) {
  Recommend.findOne({ _id: req.params.recommend })
      .populate('posts', null, null, { limit: 5 })
      .exec(function(err, recommend) {
        res.send(recommend);
      });
};