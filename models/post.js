var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var Post = new Schema({
  recommend: { type: Schema.Types.ObjectId, ref: 'Recommend' },
  title: { type: String },
  image: { type: String },
  description: { type: String },
  summary: { type: String },
  link: { type: String }
});

module.exports = mongoose.model('Post', Post);