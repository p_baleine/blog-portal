var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var Recommend = new Schema({
  title: { type: String, required: true },
  description: { type: String },
  feeds: [{ type: String }],
  posts: [{ type: Schema.Types.ObjectId, ref: 'Post' }]
});

module.exports = mongoose.model('Recommend', Recommend);